(function() {
	// var selector = $('#file1'),
	// 	files = fileInput[0].files;
	var fileInput = {
		init: function() {
			this.$el = $('#file1');
			this.$uploadButton = $('#uploadButton');
			this.bindEvents();
		},
		bindEvents: function() {
			this.$el.on('change', this.setVar.bind(this));
			this.$uploadButton.on('click', this.uploadFile.bind(this));
		},
		setVar: function() {
			this.$allowedExtension = this.$el.data('allowed-extension');
			this.$minFileSize = this.$el.data('min-file-size');
			this.$maxFileSize = this.$el.data('max-file-size');
			this.$uploadFolder = this.$el.data('upload-folder');
			this.$previewImage = this.$el.data('preview');
			this.validate();
			
		},
		validate: function() {
			this.noError();

			var valid = true;
			//check if empty
			if(this.$el[0].files.length == 0) {
				console.log('empty');
				this.addError();
				valid = false;
			}
			
			//validate file type
			var fileMimeType = this.$el[0].files[0].type;
			if(this.$allowedExtension.indexOf(fileMimeType) === -1 || fileMimeType == '') {
				this.addError();
				console.log('wrong MIME type')
				valid = false;
			}
			//validate max file size
			var fileSize = this.$el[0].files[0].size;
			if(this.$maxFileSize > this.$maxFileSize*1024*1024) {
				console.log('wrong max file size');
				this.addError();
				valid = false;
			}

			//validate min filesize
			if(typeof this.$minFileSize !== 'undefined' && this.$minFileSize > this.$minFileSize*1024) {
				this.addError();
				console.log('wrong min file size');
				valid = false;
			}

			if(valid && this.$previewImage == 1) {
				this.$el.parent().prev().attr('src', window.URL.createObjectURL(this.$el[0].files[0]));
				this.$el.parent().prev().removeClass('hidden');
			}

			return true;

		},
		uploadFile: function(e) {
			e.preventDefault();
			this.validate();
			var hasError = this.hasError();
			if(!hasError) {
				
    			var formdata = new FormData($('.uploadForm')[0]);
    			
				var request = new XMLHttpRequest();
				request.open("POST", "ajax/uploadFile.php");
				request.send(formdata);
				request.onload = function(oEvent) {
				    if (request.status == 200) {
				      console.log("Uploaded!");
				    } else {
				      console.log("Error " + request.status + " occurred when trying to upload your file.<br \/>");
				    }
				};
			}
				
		},
		addError: function() {
			this.$el.parent().parent().addClass('error');
			$('.error-text').text('you have error').removeClass('hidden');
		},
		hasError: function() {
			if(this.$el.parent().parent().hasClass('error'))
				return true;
			else
				return false
		},
		noError: function() {
			this.$el.parent().parent().removeClass('error');
			$('.error-text').text('').addClass('hidden');
			this.$el.parent().prev().attr('src', '');
			this.$el.parent().prev().addClass('hidden');
		}
	}

	fileInput.init();
})();
