<?php

class File {
    /**
     * @var string the original name of the file being uploaded
     */
    public $name;
    /**
     * @var string the path of the uploaded file on the server.
     */
    public $tmp_name;
    /**
     * @var string the MIME-type of the uploaded file (such as "image/gif").
     */
    public $type;
    /**
     * @var integer the actual size of the uploaded file in bytes
     */
    public $size;
    /**
     * @var integer an error code describing the status of this file uploading.
     */
    public $error;
    /**
     * @var setting up file.
     */
    public $file;

    public function __construct($name)
    {
        $this->loadFileAttr($name);
    }
    /**
    * @var set up file attribute
    */
    protected function loadFileAttr($name){

        if(isset($_FILES[$name]) && is_array($_FILES[$name])) {

            foreach ($_FILES[$name] as $key => $value) {
  
                $this->$key = $value;
            }
            
        } else {

            var_dump('pucpuruc');
        }

    }
    /**
    * @var validate file before upload
    */
    public function validate() {
        if(!$this->checkError())
            return false;
        if(!$this->checkExtension())
            return false;
        if(!$this->checkSize())
            return false;
        return true;
    }
    /**
    *
    */
    protected function checkError() {

    }
    /**
    *
    */
    protected function checkExtension() {
        
    }
    /**
    *
    */
    protected function checkSize() {
        
    }

    /**
     * @return string original file base name
     */
    public function getBaseName()
    {
        return pathinfo($this->name, PATHINFO_FILENAME);
    }

    /**
     * @return string file extension
     */
    public function getExtension()
    {
        return strtolower(pathinfo($this->name, PATHINFO_EXTENSION));
    }

    /**
     * @return boolean whether there is an error with the uploaded file.
     * Check [[error]] for detailed error code information.
     */
    public function getHasError()
    {
        return $this->error != UPLOAD_ERR_OK;
    }
    /**
    * @var $file 
    * @var $deleteTemFile
    * save
    */
    public function saveAs($file, $deleteTempFile = true)
    {
        if ($this->error == UPLOAD_ERR_OK) {
            if ($deleteTempFile) {
                return move_uploaded_file($this->tmp_name, $file);
            } elseif (is_uploaded_file($this->tmp_name)) {
                return copy($this->tmp_name, $file);
            }
        }
        return false;
    }

    
}